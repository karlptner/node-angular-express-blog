# Angular Express Seed Example App

Based on the Angular Express Seed, this simple app illustrates how to use [AngularJS](http://angularjs.org/) and [Express](http://expressjs.com/) on a [Node.js](http://nodejs.org/) server to make a simple blog.